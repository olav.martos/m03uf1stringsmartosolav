﻿/*
 * AUTHOR: Olav Martos
 * DATE: 30/11/2022
 * DESCRIPTION: Conjunt de metodes enlaçats per un menu per treballar amb strings. 
*/

using System;
using static System.Net.Mime.MediaTypeNames;

namespace Cadenes
{
    class Cadenes
    {
        static void Main(string[] args)
        {
            var menu = new Cadenes();
            menu.Menu();
        }

        public void Menu()
        {
            var option = "";
            do
            {
                Console.Clear();
                Console.WriteLine("0. - Sortir del menu");
                Console.WriteLine("1. - AreEquals");
                Console.WriteLine("2. - AreEquals2Part");
                Console.WriteLine("3. - CharPurge");
                Console.WriteLine("4. - CharacterReplace");
                Console.WriteLine("5. - HammingDistance");
                Console.WriteLine("6. - Parenthesis");
                Console.WriteLine("7. - Palindrom");
                Console.WriteLine("8. - WordInverse");
                Console.WriteLine("9. - WordInverse2Part");
                Console.WriteLine("A. - HelloBye");
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        AreEquals();
                        break;
                    case "2":
                        Console.Clear();
                        AreEquals2Part();
                        break;
                    case "3":
                        Console.Clear();
                        CharPurge();
                        break;
                    case "4":
                        Console.Clear();
                        CharacterReplace();
                        break;
                    case "5":
                        Console.Clear();
                        HammingDistance();
                        break;
                    case "6":
                        Console.Clear();
                        Parenthesis();
                        break;
                    case "7":
                        Console.Clear();
                        Palindrom();
                        break;
                    case "8":
                        Console.Clear();
                        WordInverse();
                        break;
                    case "9":
                        Console.Clear();
                        WordInverse2Part();
                        break;
                    case "A":
                        Console.Clear();
                        HelloBye();
                        break;
                    case "0":
                        Console.Clear();
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta!");
                        break;
                }
                Console.ReadLine();
            } while (option != "0");
        }



        /*Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals.*/
        public void AreEquals()
        {
            Console.Write("Introdueix la primera secuencia de caracters: ");
            string str1 = Console.ReadLine();
            Console.Write("Introdueix la segona secuencia de caracters: ");
            string str2 = Console.ReadLine();

            if (str1 == str2) Console.WriteLine("Son iguals");
            else Console.WriteLine("No son iguals");
        }



        /*Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals, sense tenir en compte majúscules i minúscules.*/
        public void AreEquals2Part()
        {
            Console.Write("Introdueix la primera secuencia de caracters: ");
            string str1 = Console.ReadLine();
            Console.Write("Introdueix la segona secuencia de caracters: ");
            string str2 = Console.ReadLine();

            if (str1.ToLower() == str2.ToLower())
            {
                Console.WriteLine("Son iguals");
            }
            else
            {
                Console.WriteLine("No son iguals");
            }
        }



        /*Feu un programa que rebi per l’entrada 1 String i després una serie indefinida de caràcters per anar traient de l’String, si és que el conté, fins que es rep un 0.
         * Entrada
         * L’entrada consisteix en un String seguit de caràcters individuals, fins a la introducció d’un 0.
         * Sortida
         * Per la sortida heu d’imprimir l’String resultat de treure els caràcters.
         */
        public void CharPurge()
        {
            Console.Write("Introdueix la paraula que vols utilitzar: ");
            string word = Console.ReadLine();

            char newChar = '\0'; // Degut a que no es pot fer '' per a que no tingui contingut, s'introdueix aquest codi per a que aparegui buit i el programa funcioni correctament

            Console.Write("Lletra que vols eliminar: ");
            char oldChar = Convert.ToChar(Console.ReadLine());
            while (oldChar != '0')
            {
                word = word.Replace(oldChar, newChar);
                Console.Write("Lletra que vols eliminar: ");
                oldChar = Convert.ToChar(Console.ReadLine());
            }

            Console.WriteLine(word);
        }



        /*Feu un programa que rebi per l’entrada 1 seqüència de caràcters i just després dos caràcters, el primer per substituir de l’String pel segon.
         * Entrada
         * L’entrada consisteix en un String i dos caràcters.
         * Sortida
         * Per la sortida heu d’imprimir l’String amb el caràcter reemplaçat.
         */
        public void CharacterReplace()
        {
            Console.Write("Introdueix la paraula que vols utilitzar: ");
            string word = Console.ReadLine();

            Console.Write("Lletra que vols substituir: ");
            char oldChar = Convert.ToChar(Console.ReadLine());
            Console.Write("Lletra que vols introduir: ");
            char nuevo = Convert.ToChar(Console.ReadLine());

            Console.WriteLine(word.Replace(oldChar, nuevo));

        }



        /*Feu un programa que rebi per l’entrada 2 seqüències d’ADN i calculi la distància d’Hamming entre elles. 
         * Tingueu en compte que per poder realitzar aquest càlcul heu d’assegurar que les cadenes tinguin exactament la mateixa mida.*/
        public void HammingDistance()
        {
            Console.Write("Escriu la primera cadena d'ADN: ");
            string dna1 = Console.ReadLine();
            Console.Write("Escriu la segona cadena d'ADN: ");
            string dna2 = Console.ReadLine();

            int hamming = 0;

            if (dna1.Length != dna2.Length)
            {
                Console.WriteLine("Entrada no valida");
            }
            else
            {
                for(int i = 0; i < dna1.Length; i++)
                {
                    if (dna1[i] != dna2[i])
                    {
                        hamming++;
                    }
                }
            }
            Console.WriteLine($"La distancia d'Hamming d'aquestes dues cadenes es: {hamming}");
        }



        /*Feu una aplicació que donada una seqüència amb només ‘(’ i ‘)’, digueu si els parèntesis tanquen correctament.*/
        public void Parenthesis()
        {
            Console.WriteLine("Introduce la cadena de parentesis");
            string chain = Console.ReadLine();
            int cont = 0;

            for(int i = 0; i < chain.Length; i++)
            {
                if (i == 0)
                {
                    if (chain[i] == ')')
                    {
                        cont++;
                    }
                }
                if (chain[i] == '(')
                {
                    cont++;
                }
                else if(chain[i]==')')
                {
                    cont--;
                }

                if (i==chain.Length-1)
                {
                    if (chain[i]=='(') 
                    {
                        cont++;
                    }
                }
            }

            if (cont == 0)
            {
                Console.WriteLine("si");
            }
            else
            {
                Console.WriteLine("no");
            }

        }



        /*Feu un programa que indiqui si una paraula és un palíndrom o no. Recordeu que una paraula és un palíndrom si es llegeix igual d’esquerra a dreta que de dreta a esquerra.*/
        public void Palindrom()
        {
            string palindrome = "";
            Console.Write("Escriu la paraula per saber si es palindrom: ");
            string word = Console.ReadLine().ToLower();
            for (int i = word.Length - 1; i >= 0; i--)
            {
                palindrome += word[i];
            }
            if (palindrome == word)
            {
                Console.WriteLine("Es un palindrom");
            }
            else
            {
                Console.WriteLine("No es un palindrom.");
            }
        }



        /* Feu un programa que llegeixi paraules, i que escrigui cadascuna invertint l’ordre dels seus caràcters. */
        public void WordInverse()
        {
            Console.Write("Escriu quantes paraules vols invertir: ");
            int num = Convert.ToInt32(Console.ReadLine());
            string[] inverse = new string[num];

            for(int i = 0; i < inverse.Length; i++)
            {
                Console.Write("Escriu una paraula: ");
                string str = Console.ReadLine();


                char[] chars = str.ToCharArray();
                Array.Reverse(chars);
                inverse[i] = new string(chars);
            }
            for(int j = 0; j < inverse.Length; j++)
            {
                Console.WriteLine(inverse[j]);
            }

        }

        /*Feu un programa que llegeixi un String i inverteixi l’ordre de les paraules dins del mateix.*/
        public void WordInverse2Part()
        {
            Console.WriteLine("Escriu la teva cadena");
            string phrase = Console.ReadLine();
            string[] words = phrase.Split(' ');
            int index = words.Length;
            string[] reverse = new string[index];

            for(int i = index - 1, j=0; i > -1; i--, j++)
            {
                reverse[j] = words[i];
            }

            for(int i = 0; i < index; i++)
            {
                Console.Write($"{reverse[i]} ");
            }
        }



        /*
         * Feu un programa que llegeixi una seqüència de lletres acabada en punt i digui si conté la successió de lletres consecutives ‘h’,‘o’, ‘l’, ‘a’ o no.
         * Entrada
         * L’entrada consisteix en una seqüència de lletres minúscules acabada en ‘.’.
         * Sortida
         * Cal escriure “hola” si l’entrada conté les lletres ‘h’, ‘o’, ‘l’, ‘a’ consecutivament. Altrament, cal escriure “adeu”.
         */
        public void HelloBye()
        {
            Console.Write("Escriu la teva linea, ha de finalitzar amb un punt: ");
            string line = Console.ReadLine();
            int end = line.Length - 1;
            while (line[end] != '.')
            {
                Console.Write("Continuacio de la linea. Si no saps com sortir del bucle, mira la primera linea de la terminal: ");
                line += Console.ReadLine();
                end=line.Length - 1;
            }
            for(int i = 0; i < line.Length; i++)
            {
                if (line[i] == 'h')
                {
                    if (line[i + 1] == 'o')
                    {
                        if (line[i + 2] == 'l'){
                            if (line[i + 3] == 'a') Console.WriteLine("Hola");
                            else Console.WriteLine("Adeu");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Adeu");
                    }
                }
            }

        }
    }
}



